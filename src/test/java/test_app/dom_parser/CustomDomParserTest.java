package test_app.dom_parser;

import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CustomDomParserTest {

    @Test
    void extractX1X2A1ValuesFileIsNull() {
        assertThrows(NullPointerException.class, () -> new CustomDomParser(null));
    }

    @Test
    void extractX1X2A1ValuesFileDontExist() {
        File file = new File("NotExistingFile.test");
        assertThrows(FileNotFoundException.class, () -> new CustomDomParser(file));
    }

    @Test
    void extractX1X2A1ValuesEmptyFile() {
        File file = new File("EmptyFile.txt");
        assertThrows(SAXParseException.class, () -> new CustomDomParser(file));
    }

    @Test
    void extractX1X2A1ValuesNonValidFile() {
        File file = new File("someText.txt");
        assertThrows(SAXParseException.class, () -> new CustomDomParser(file));
    }

    @Test
    void extractX1X2A1ValuesNoResult() throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
        File file = new File("sample0.xml");
        final List<String> expected = Collections.emptyList();
        final CustomDomParser customDomParser = new CustomDomParser(file);
        final List<String> actual = customDomParser.extractX1X2A1Values();

        assertEquals(expected, actual, "Both lists should be empty");
    }

    @Test
    void extractX1X2A1Values3Result() throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
        File file = new File("sample1.xml");
        final List<String> expected = Arrays.asList("a", "c", "h");
        final CustomDomParser customDomParser = new CustomDomParser(file);
        final List<String> actual = customDomParser.extractX1X2A1Values();

        assertEquals(expected, actual, "Lists should match");
    }

    @Test
    void extractX1X2A1ValuesNested() throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
        File file = new File("sample2.xml");
        final List<String> expected = Collections.singletonList("test");
        final CustomDomParser customDomParser = new CustomDomParser(file);
        final List<String> actual = customDomParser.extractX1X2A1Values();

        assertEquals(expected, actual, "Lists should match");
    }
}
