package test_app;

import test_app.dom_parser.CustomDomParser;
import test_app.utils.Printer;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;
import java.util.List;

/*
Given the following XML document (see attachment),
write a Java program that outputs the
values of nodes with name “x2” and id=“a1”
that are direct children of nodes with name “x1”
 */
public class App {
    public static void main(String[] args){
        System.out.println();

       final File parsedFile = new File("sample1.xml");
        try {
            final CustomDomParser customDomParser = new CustomDomParser(parsedFile);
            final List<String> strings = customDomParser.extractX1X2A1Values();
            Printer.printResults(strings);

        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
            e.printStackTrace();
        }

    }
}
