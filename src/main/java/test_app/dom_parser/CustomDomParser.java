package test_app.dom_parser;


import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CustomDomParser {
    private final Document document;
    private final XPath xPath;

    public CustomDomParser(File xmlFile) throws ParserConfigurationException, IOException, SAXException {
        Objects.requireNonNull(xmlFile, "Xml file cannot be null");
        if (!xmlFile.exists()) {
            throw new FileNotFoundException("Xml file not found");
        }
        document = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(xmlFile);

        xPath = XPathFactory
                .newInstance()
                .newXPath();
    }

    public List<String> extractX1X2A1Values() throws XPathExpressionException {
        final String xPathExpression = "//x1/x2[@id='a1']";
        final XPathExpression compiledExpression = xPath.compile(xPathExpression);
        final NodeList evaluateNodeSet = (NodeList) compiledExpression.evaluate(document, XPathConstants.NODESET);

        final List<String> result = new ArrayList<>(evaluateNodeSet.getLength());
        for (int i = 0; i < evaluateNodeSet.getLength(); i++) {
            Node node = evaluateNodeSet
                    .item(i)
                    .getFirstChild();
            // If node has no value or is not a text, just skip it
            while ((node.getNodeType() != Node.TEXT_NODE || node.getNodeValue().trim().equals("")) && node.getNextSibling() != null) {
                node = node.getNextSibling();
            }
            final String nodeValue = node.getNodeValue().trim();
            result.add(nodeValue);
        }

        return result;
    }
}
