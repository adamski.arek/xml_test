package test_app.utils;

import java.util.Collection;

public abstract class Printer {

    static public void printResults(Collection<String> collection) {
        System.out.println("The values of x2[id=a1], which are children of x1:");
        collection.forEach(x -> System.out.println("\t".concat(x)));
        System.out.println();
    }
}
